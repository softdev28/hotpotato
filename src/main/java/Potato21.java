
import java.util.ArrayList;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ACER
 */
public class Potato21 {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        System.out.println("Enter number of children: ");

        int n = kb.nextInt();
        ArrayList<String> list = new ArrayList<String>();

        for (int i = 1; i <= n; i++) {
            list.add("Children" + i);
        }
        System.out.println("Enter k: ");
        int k = kb.nextInt();
         System.out.println(HotPotato(list, k) + " is the winner.");
    }

    public static String HotPotato(ArrayList<String> list, int k) {
        while (list.size() > 1) {
            int count = 0;
            for (int i = 1; i < k; i++) {
                count++;
                if (count > list.size() - 1) {
                    count = 0;
                }
            }
       
            list.remove(count);
        }
        return list.toString();
}
}
